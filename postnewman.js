#!/usr/bin/env node

var Newman = require('newman') ;
var fs = require('fs') ;
var mandrill = require('mandrill-api/mandrill') ;
		
var argv = require('minimist')(process.argv.slice(2)) ;
var errors = [] ;

if ( !argv.collection ) {
	errors.push('--collection is missing') ;
}

if ( !argv.environment ) {
	errors.push('--environment is missing') ;
}

if ( !argv.output ) {
	errors.push('--output is missing') ;
}

if ( !argv.notify ) {
	errors.push('--notify is missing') ;
} else {
	argv.notify = JSON.parse(argv.notify) ;
}

if ( !argv.twilioAccountSid ) {
	errors.push('--twilioAccountSid is missing') ;
}

if ( !argv.twilioAccountToken ) {
	errors.push('--twilioAccountToken is missing') ;
}

if ( !argv.twilioFromNumber ) {
	errors.push('--twilioFromNumber is missing') ;
}

if ( !argv.mandrillApiKey ) {
	errors.push('--mandrillApiKey is missing') ;
}

if ( !argv.mandrillFromEmail ) {
	errors.push('--mandrillFromEmail is missing') ;
}

if ( !argv.mandrillFromName ) {
	errors.push('--mandrillFromName is missing') ;
}


if ( errors.length > 0 ) {
	console.dir({ errors: errors }) ;	
} else {
	var collection = JSON.parse(fs.readFileSync(argv.collection, 'utf8')) ;
	var environment = JSON.parse(fs.readFileSync(argv.environment, 'utf8')) ;
	var newmanOptions = {
		envJson: environment ,
		iterationCount: 1,                    // define the number of times the runner should run 
		outputFile: argv.output,            // the file to export to 
		responseHandler: "TestResponseHandler", // the response handler to use 
		asLibrary: true,         				// this makes sure the exit code is returned as an argument to the callback function 
		stopOnError: false
	}
	//console.dir(collection) ;
	//console.dir(environment) ;
	Newman.execute(collection, newmanOptions, function(exitCode){
		var result = { exitCode: exitCode } ;
		/* send notifications */ 
		var results = JSON.parse(fs.readFileSync(argv.output, 'utf8')) ;
		resultsResults = results.results ;
		tests = resultsResults[0].tests ;
		collectionName = results.collection.name ;
		
		if ( result.exitCode != 0 || argv.assumeTestsFailed) {
			if ( argv.verbose ) console.log('sending notifications') ;
			
			if ( argv.verbose ) console.dir( { tests: tests } ) ;
			if ( argv.verbose ) console.dir(argv.notify) ;
			//require the Twilio module and create a REST twilio
			var twilio = require('twilio')(argv.twilioAccountSid, argv.twilioAccountToken);
			var mandrillClient = new mandrill.Mandrill('R835GWry1PyIFZY7YqS-Kw');
			var mandrillParams = { 
				"message": {
				  "from_email": argv.mandrillFromEmail,
				  "from_name": argv.mandrillFromName,
				  "to":null,
				  "subject": collectionName,
					"html": JSON.stringify(tests)
				}
			};
			
			mandrillTo = [] ;
			
			for ( key in argv.notify ) {
				var notify = argv.notify[key] ;
				if (notify.sms) {
					// console.dir(notify) ;	
					twilio.sendMessage({

					    to: notify.sms , // Any number Twilio can deliver to
					    from: argv.twilioFromNumber, // A number you bought from Twilio and can use for outbound communication
					    body: JSON.stringify(tests) // body of the SMS message

					}, function(err, responseData) { //this function is executed when a response is received from Twilio

					    if (!err) { // "err" is an error received during the request, if any

					        // "responseData" is a JavaScript object containing data received from Twilio.
					        // A sample response from sending an SMS message is here (click "JSON" to see how the data appears in JavaScript):
					        // http://www.twilio.com/docs/api/rest/sending-sms#example-1

					        console.log(responseData.from); // outputs "+14506667788"
					        console.log(responseData.body); // outputs "word to your mother."

					    } else {
					    	console.dir({ error: err }) ;
					    }
					}) ;		
				}
				
				if( notify.email ) {
					mandrillTo.push(notify) ;
				}
				
			}
			if ( mandrillTo.length > 0) {
				//console.log('sending notification emails') ;
				mandrillParams.message.to = mandrillTo ;
				//console.dir(mandrillParams) ;
				mandrillClient.messages.send(mandrillParams, function(res) {
						if ( argv.verbose ) console.log('response from mandrill') ;
				    if ( argv.verbose ) console.dir({mandrillResponse: res });
				}, function(err) {
						if ( argv.verbose ) console.log('error from mandrill') ;
				    console.dir({mandrillError: err });
				});				
			}
		} else {
			if ( argv.verbose ) console.log( collectionName + ': all tests succeeded.') ;
		}
	});
}
