postnewman is designed to send out mandrill email and twilio sms notifications in the event of a newman test failing.

Written in response to one of our web sites being attacked by script kiddies.

/* example call which would be on one line */

./postnewman.js 

	--collection postman-tests/REF-584-test-if-web-site-has-been-hacked.json.postman_collection 
	--environment postman-tests/REF-584-website.postman_environment 
	--notify '[{"email":"some.person@somedomain.com","name":"Some Name"},{"sms":"valid sms number","name":"Some name"}]' 
	--output postman-tests/REF-584-test-if-web-site-has-been-hacked-result.json 
	--twilioAccountSid 'your twilio account sid' 
	--twilioAccountToken 'your twilio account token' 
	--twilioFromNumber 'your twilio from number' 
	--mandrillApiKey 'your mandrill api key' 
	--mandrillFromEmail 'your mandril from email address' 
	--mandrillFromName 'your mandrill from name' 
	--verbose 0 
	--assumeTestsFailed 0
	
explanation of parameters. parameters are case sensitive.

	--collection: path to a valid postman test collection
	--environment: path to a valid postman test environment
	--notify: a valid json string list of people to notify. email messages for via mandrill. sms messages go via twilio.
	--output: valid path where the results will be stored. Used when constructing outgoing messages.
	--twilioAccountSid: 'your twilio account sid' 
	--twilioAccountToken: 'your twilio account token' 
	--twilioFromNumber: 'your twilio from number' 
	--mandrillApiKey: 'your mandrill api key' 
	--mandrillFromEmail: 'your mandril from email address' 
	--mandrillFromName: 'your mandrill from name' 
	--verbose: 0 to ignore. 1 to see lots of messages on the command line console. 
	--assumeTestsFailed: 0 to ignore. 1 to simulate a failed test.